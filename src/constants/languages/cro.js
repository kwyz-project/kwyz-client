/* This file contains strings used throughout the app.
*  There's a file like this for every language.
*  JS file should be imported were strings are used.
*/ 

// Dashboard page
export const dashboard_welcome = 'Dobrodošli na Kwyz.';
export const dashboard_welcome_sub = 'Klikni na Kvizovi te otkrij nove kvizove! Možeš kreirati svoje kvizove na Kreiraj stranici te urediti postojeće na Uredi.';
export const dashboard_quizzes_head = 'Kvizovi';
export const dashboard_quizzes_body = 'Biraj među tisućama kvizova.';
export const dashboard_create_head = 'Kreiraj';
export const dashboard_create_body = 'Stvori vlastiti kviz.';
export const dashboard_modify_head = 'Uredi';
export const dashboard_modify_body = 'Uredi postojeći kviz.';
export const dashboard_contribute_head = 'Doprinesi';
export const dashboard_contribute_body = 'Kontaktiraj naš razvojni tim ukoliko želiš pomoći s unaprijeđenjem Kwyz-a.';

// Quizzes page
export const quizzes_search = 'Traži'
export const quizzes_question = 'Što je prikazano na videu?'
export const quizzes_congratulations = 'Čestitke, kviz riješen!'
export const quizzes_play_again = 'Ponovi'
export const quizzes_menu = 'Početna'

// Create page
export const create_general = 'Glavne informacije';
export const create_name = 'Ime kviza';
export const create_desc = 'Opis kviza';
export const create_name_ph = 'Ime';
export const create_desc_ph = 'Opis';
export const create_videos = 'Videi i odgovori';
export const create_question = 'Pitanje';
export const create_video = 'Url videa';
export const create_answers = 'Odgovori';
export const create_answer = 'Odgovor';
export const create_answera = 'Odgovor A';
export const create_answerb = 'Odgovor B';
export const create_answerc = 'Odgovor C';
export const create_answerd = 'Odgovor D';
export const create_correct = 'Odaberi točan odgovor';
export const create_newq = 'Dodaj novo pitanje';
export const create_complete = 'Završi';
