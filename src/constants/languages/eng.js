/* This file contains strings used throughout the app.
*  There's a file like this for every language.
*  JS file should be imported were strings are used.
*/ 

// Dashboard page
export const dashboard_welcome = 'Welcome to Kwyz.';
export const dashboard_welcome_sub = 'Head over to Quizzes page and discover new quizzes! You can create your own at the Create page and modify your quizzes at Modify.';
export const dashboard_quizzes_head = 'Quizzes';
export const dashboard_quizzes_body = 'Choose from millions of quizzes.';
export const dashboard_create_head = 'Create';
export const dashboard_create_body = 'Add your own quizz.';
export const dashboard_modify_head = 'Modify';
export const dashboard_modify_body = 'Update your quizz.';
export const dashboard_contribute_head = 'Contribute.';
export const dashboard_contribute_body = 'Reach out to our development team and help us improve.';

// Quizzes page
export const quizzes_search = 'Search'
export const quizzes_question = 'Question'
export const quizzes_congratulations = 'Congratulations, quiz completed!'
export const quizzes_play_again = 'Play again'
export const quizzes_menu = 'Main menu'

// Create page
export const create_general = 'General information';
export const create_name = 'Quizz name';
export const create_desc = 'Quizz description';
export const create_name_ph = 'Name';
export const create_desc_ph = 'Description';
export const create_videos = 'Videos and answers';
export const create_question = 'Question';
export const create_video = 'Quizz video url';
export const create_answers = 'Answers';
export const create_answera = 'Answer A';
export const create_answerb = 'Answer B';
export const create_answerc = 'Answer C';
export const create_answerd = 'Answer D';
export const create_correct = 'Select correct Answer';
export const create_newq = 'Add new question';
export const create_complete = 'Complete';