import React, { useState } from 'react';
import { Row, CardBody, Card, CardHeader, Col, Form, FormGroup, Label, Input } from 'reactstrap';
import { createYoutubeUrl, getRandomColor } from '../helpers/helper';

import * as constants from '../../constants/languages/cro'

const Question = props => {
    const [bodyVisible, setBodyVisible] = useState(true);
    const [color] = useState(getRandomColor());

    const videoUrl = props.question.url ? createYoutubeUrl(props.question.url) : null;

    return (
        <Card className="" style={{ borderStyle: 'solid' }}>
            <CardHeader className={'bg-' + color} onClick={() => setBodyVisible(!bodyVisible)} style={{ cursor: 'pointer' }}>
                <p style={{ color: 'white', textAlign: 'center' }}>
                    {constants.create_question} {props.id}
                    <span className="float-right" data-effect="fadeOut">
                        {bodyVisible && (
                            <i className="fa fa-angle-down fa-2x"></i>
                        )}
                        {!bodyVisible && (
                            <i className="fa fa-angle-up fa-2x"></i>
                        )}
                    </span>
                </p>
            </CardHeader>
            {bodyVisible && (
                <CardBody>
                    <Form>
                        <FormGroup>
                            <Label for="url">{constants.create_video}</Label>
                            <Input
                                type="text"
                                name="url"
                                id="url"
                                value={props.question.url}
                                placeholder="http://www.kywz.io"
                                onChange={event => props.onChange(props.id, event.target.name, event.target.value)}
                            />
                            {videoUrl && (
                                <div className="embed-responsive embed-responsive-16by9">
                                    <iframe
                                        title="video"
                                        className="embed-responsive-item"
                                        src={videoUrl}
                                        style={{ pointerEvents: 'none' }}
                                    ></iframe>
                                </div>
                            )}
                            <Label for="answers" className="pt-3">
                                {' '}
                                {constants.create_answers}{' '}
                            </Label>
                            <FormGroup>
                                <Row className="justify-content-center">
                                    <Col md="6" className="pt-3">
                                        <Input
                                            type="text"
                                            name="answer1"
                                            id="answer1"
                                            value={props.question.answer1}
                                            placeholder={constants.create_answera}
                                            onChange={event =>
                                                props.onChange(props.id, event.target.name, event.target.value)
                                            }
                                        />
                                    </Col>
                                    <Col md="6" className="pt-3">
                                        <Input
                                            type="text"
                                            name="answer2"
                                            id="answer2"
                                            value={props.question.answer2}
                                            placeholder={constants.create_answerb}
                                            onChange={event =>
                                                props.onChange(props.id, event.target.name, event.target.value)
                                            }
                                        />
                                    </Col>
                                </Row>
                                <Row className="justify-content-center pt-2">
                                    <Col md="6" className="pt-3">
                                        <Input
                                            type="text"
                                            name="answer3"
                                            id="answer3"
                                            value={props.question.answer3}
                                            placeholder={constants.create_answerc}
                                            onChange={event =>
                                                props.onChange(props.id, event.target.name, event.target.value)
                                            }
                                        />
                                    </Col>
                                    <Col md="6" className="pt-3">
                                        <Input
                                            type="text"
                                            name="answer4"
                                            id="answer4"
                                            value={props.question.answer4}
                                            placeholder={constants.create_answerd}
                                            onChange={event =>
                                                props.onChange(props.id, event.target.name, event.target.value)
                                            }
                                        />
                                    </Col>
                                </Row>
                                <Row className=" pt-2">
                                    <Col md="6" className="pt-3">
                                        <Label for="exampleSelect">{constants.create_correct}</Label>
                                        <Input
                                            type="select"
                                            name="correctAnswer"
                                            id="correctAnswerSelect"
                                            value={props.question.correctAnswer}
                                            onChange={event =>
                                                props.onChange(props.id, event.target.name, event.target.value)
                                            }
                                        >
                                            <option>A</option>
                                            <option>B</option>
                                            <option>C</option>
                                            <option>D</option>
                                        </Input>
                                    </Col>
                                </Row>
                            </FormGroup>
                        </FormGroup>
                    </Form>
                </CardBody>
            )}
        </Card>
    );
};

export default Question;
