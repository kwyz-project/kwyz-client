import React from 'react';
import { CardSubtitle, CardImg, CardBody, Card, CardTitle } from 'reactstrap';

const Quiz = (props) => {
    return (
        <div style={{ padding: "50px" }}>
            <Card tag="a" onClick={() => props.onClick(props.id)} style={{ width: "400px", height: "400px", cursor: "pointer"}}>
                <CardImg src={props.imageUrl} top width="100%" alt="QUIZ" style={{ width: "400px", height: "350px" }}/>
                <CardBody>
                    <CardTitle>
                        {props.title}
                    </CardTitle>
                    <CardSubtitle>
                        {props.description}
                    </CardSubtitle>
                </CardBody>
          </Card>            
        </div>
    );
}

export default Quiz;