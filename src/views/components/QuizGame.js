import React, { useState } from 'react';
import { Row, CardSubtitle, CardHeader, CardBody, Card, Button } from 'reactstrap';
import ReactStoreIndicator from 'react-score-indicator'
import _ from 'lodash';

import * as constants from '../../constants/languages/cro';

import { createYoutubeUrl } from '../helpers/helper'

const QuizGame = (props) => {
  const [currentQuestion, setCurrentQuestion] = useState(1);
  const [score, setScore] = useState(0);

  var colors = ['primary', 'secondary', 'success', 'warning', 'danger', 'info'];

  const aColor = colors[Math.floor(Math.random() * colors.length)];
  colors = _.without(colors, aColor);
  const bColor = colors[Math.floor(Math.random() * colors.length)];
  colors = _.without(colors, bColor);
  const cColor = colors[Math.floor(Math.random() * colors.length)];
  colors = _.without(colors, cColor);
  const dColor = colors[Math.floor(Math.random() * colors.length)];

  function answerClicked(answer) {
    const correctAnswer = props.questions[currentQuestion - 1].correctAnswer;

    if (correctAnswer === answer) {
      setScore(score + 1);
    }

    setCurrentQuestion(currentQuestion + 1);
  }

  function restartQuizz() {
      setCurrentQuestion(1);
      setScore(0);
  }

  return (
    <div>
        {currentQuestion <= props.questions.length &&
        <Card style={{ maxWidth: '1200px', maxHeight: '1000px', minHeight: '500px', margin: '0 auto' }}>
        <CardHeader className="card-header-primary" style={{ alignItems: 'center', padding: '10px' }}>
          <span style={{ fontSize: '250%', color: 'white' }}>{constants.quizzes_question}</span>
          <span
            className="float-right"
            data-effect="fadeOut"
            style={{ cursor: 'pointer', paddingBottom: '10px', paddingTop: '5px' }}
          >
            <i className="fa fa-close fa-3x" onClick={() => props.resetComponent(null)}></i>
          </span>
        </CardHeader>
        <CardBody style={{ padding: '0px' }}>
          <div className="embed-responsive embed-responsive-16by9">
            <iframe
              title="video"
              className="embed-responsive-item"
              src={createYoutubeUrl(props.questions.find(x => x.id === currentQuestion).url)}
              style={{ pointerEvents: 'none' }}
            ></iframe>
          </div>
          <CardSubtitle></CardSubtitle>
        </CardBody>
        <CardBody className="card-header-primary" style={{ paddingTop: '40px', paddingBottom: '40px' }}>
          <Row className="justify-content-center">
            <Button
              className="mr-5 ml-5 mb-1"
              color={aColor}
              size="lg"
              style={{ width: '200px' }}
              onClick={() => answerClicked('A')}
            >
              {' '}
              {props.questions.find(x => x.id === currentQuestion).answer1}{' '}
            </Button>
            <Button
              className="mr-5 ml-5 mb-1"
              color={bColor}
              size="lg"
              style={{ width: '200px' }}
              onClick={() => answerClicked('B')}
            >
              {' '}
              {props.questions.find(x => x.id === currentQuestion).answer2}{' '}
            </Button>
          </Row>
          <Row className="justify-content-center mt-3">
            <Button
              className="mr-5 ml-5 mb-1"
              color={cColor}
              size="lg"
              style={{ width: '200px' }}
              onClick={() => answerClicked('C')}
            >
              {' '}
              {props.questions.find(x => x.id === currentQuestion).answer3}{' '}
            </Button>
            <Button
              className="mr-5 ml-5 mb-1"
              color={dColor}
              size="lg"
              style={{ width: '200px' }}
              onClick={() => answerClicked('D')}
            >
              {' '}
              {props.questions.find(x => x.id === currentQuestion).answer4}{' '}
            </Button>
          </Row>
        </CardBody>
      </Card>
        }
        {currentQuestion > props.questions.length &&
        <Card style={{ maxWidth: '1200px', maxHeight: '1000px', minHeight: '500px', margin: '0 auto' }}>
            <CardHeader className="card-header-primary" style={{ textAlign: 'center', padding: '10px', paddingTop: '50px' }}>
                <h1 style={{ color: 'white' }}>{constants.quizzes_congratulations}</h1>
            </CardHeader>        
            <CardBody className="card-header-primary" style={{ textAlign: 'center', paddingTop: '100px' }}>
                <ReactStoreIndicator
                    value={score}
                    maxValue={props.questions.length}
                />
                <Row className="justify-content-center mt-3" style={{ paddingTop: '100px', paddingBottom: '100px'}}>
                    <Button
                        className="mr-5 ml-5 mt-1 mb-1"
                        color="info"
                        size="lg"
                        style={{ width: '200px' }}
                        onClick={() => restartQuizz()}>
                        {constants.quizzes_play_again}
                    </Button>
                    <Button
                        className="mr-5 ml-5 mt-1 mb-1"
                        color="info"
                        size="lg"
                        style={{ width: '200px' }}
                        onClick={() => props.resetComponent(null)}>
                        {constants.quizzes_menu}
                    </Button>
                </Row>
            </CardBody>
        </Card>
        }
    </div>
  );
};

export default QuizGame;
