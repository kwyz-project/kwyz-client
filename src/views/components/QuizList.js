import React from 'react';
import { Row, Input, Col } from 'reactstrap';
import Quiz from '../components/Quiz'

import * as constants from '../../constants/languages/cro';

const QuizList = (props) => {
    return (
        <div>
            <Row className="justify-content-center">
                <Col md={4}>
                    <Input 
                        placeholder={constants.quizzes_search}
                        style={{ border: 0, borderRadius: "25px" }}
                        onChange={e => props.onFilter(e.target.value)}/>
                </Col>
            </Row>
            <div style={{ paddingTop: "40px" }}>
                <Row className="justify-content-center">
                    {props.items.map(item => (
                        <Quiz 
                            key={item.id}
                            id={item.id}
                            title={item.name} 
                            description={item.description}
                            imageUrl={item.image}
                            onClick={props.onClick}                            
                        />
                    ))}
                </Row>
            </div>
        </div>
    );
}

export default QuizList;