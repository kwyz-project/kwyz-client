export function createYoutubeUrl (url) {
    const baseUrl = 'https://www.youtube.com/embed/';
    const videoOptions = '?autoplay=1&color=white&controls=0&disablekb=1&mute=1';
    const videoKey = url.split('=').pop();

    return baseUrl + videoKey + videoOptions;
};

export function getRandomColor () {
    var colors = ['primary', 'secondary', 'success', 'warning', 'danger', 'info'];
    return colors[Math.floor(Math.random() * colors.length)];
};