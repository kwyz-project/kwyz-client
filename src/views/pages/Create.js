import React, { useState } from 'react';
import { CardHeader, CardBody, Card, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import Question from '../components/Question';
import _ from 'lodash';
import database from '../../database';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import * as constants from '../../constants/languages/cro';

const Create = () => {
    const [quizzName, setQuizzName] = useState('');
    const [quizzDescription, setQuizzDescription] = useState('');
    const [questions, setQuestions] = useState([
        {
            id: 1,
            url: '',
            answer1: '',
            answer2: '',
            answer3: '',
            answer4: '',
            correctAnswer: 'A',
        },
    ]);

    function onUpdate(id, propertyName, propertyValue) {
        var newQuestions = _.cloneDeep(questions);
        var newQuestion = newQuestions.find(x => x.id === id);

        newQuestion[propertyName] = propertyValue;
        setQuestions(newQuestions);
    }

    function addNewQuestion() {
        var newQuestions = _.cloneDeep(questions);

        newQuestions.push({
            id: questions.length + 1,
            url: '',
            answer1: '',
            answer2: '',
            answer3: '',
            answer4: '',
            correctAnswer: 'A',
        });
        setQuestions(newQuestions);
    }

    function completeSetup() {
        const image = getRandomAbstractImage();
        const payload = {
            id: Date.now(),
            name: quizzName,
            description: quizzDescription,
            image: image,
            questions: questions,
        };

        database
            .ref('quizzes')
            .push()
            .set({
                payload,
            });

        toast.success('Kviz uspješno kreiran!');
    }

    function getRandomAbstractImage() {
        const images = [
            'https://cdn.pixabay.com/photo/2020/09/03/03/43/abstract-5540113_960_720.png',
            'https://d39l2hkdp2esp1.cloudfront.net/img/eps/E4372_2x/c/E4372_01.jpg?20180703010354',
            'https://www.freevector.com/uploads/vector/preview/10960/VectorPortal-Abstract-Shapes-Layout.jpg',
            'https://c4.wallpaperflare.com/wallpaper/992/545/78/abstract-shapes-hd-wallpaper-preview.jpg',
            'https://i.pinimg.com/originals/f9/8b/14/f98b1447fa340a28079fe8dfcdf47d84.jpg',
            'https://image.freepik.com/free-vector/pastel-abstract-shapes-background_23-2148649489.jpg',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRGtF2lPU6jBxZDfTdeM3Ge_rcsgPcG4A4ohA&usqp=CAU',
        ];

        return images[Math.floor(Math.random() * images.length)];
    }

    return (
        <div>
            <Card style={{ maxWidth: '1200px', margin: '0 auto' }}>
                <CardHeader className="card-header-primary" style={{ alignItems: 'center', padding: '10px' }}>
                    <span style={{ fontSize: '150%', color: 'white', marginLeft: '7px' }}>{constants.create_general}</span>
                </CardHeader>
                <CardBody>
                    <Form>
                        <FormGroup>
                            <Label for="name">{constants.create_name}</Label>
                            <Input type="text" name="name" id="name" placeholder={constants.create_name_ph} onChange={event => setQuizzName(event.target.value)} />
                            {/* <Label for="file" className="pt-3">
                                Quizz image
                            </Label>
                            <Input
                                type="file"
                                name="file"
                                id="quizzFile"
                                onChange={event => setQuizzImage(event.target.value)}
                            /> */}
                            <Label for="name" className="pt-3">
                                {constants.create_desc}
                            </Label>
                            <Input type="textarea" name="description" id="description" placeholder={constants.create_desc_ph} onChange={event => setQuizzDescription(event.target.value)} />
                        </FormGroup>
                    </Form>
                </CardBody>
                <CardHeader className="card-header-primary" style={{ padding: '10px' }}>
                    <span style={{ fontSize: '150%', color: 'white', marginLeft: '7px' }}>{constants.create_videos}</span>
                </CardHeader>
                <CardBody>
                    {questions.map(question => (
                        <Question key={question.id} id={question.id} question={question} onChange={onUpdate} />
                    ))}
                </CardBody>
                <div className="d-flex justify-content-center pb-3">
                    <Button color="success" onClick={() => addNewQuestion()}>
                        {constants.create_newq}
                    </Button>
                </div>
            </Card>

            <div className="d-flex justify-content-center pb-3 pt-3">
                <Button color="primary" size="lg" onClick={() => completeSetup()}>
                    <i className="fa fa-check"></i>&nbsp;{constants.create_complete}
                </Button>
            </div>
            <ToastContainer />
        </div>
    );
};

export default Create;
