import React, { Component } from 'react';
import quizzesFeature from '../../assets/logos/layers.svg';
import createFeature from '../../assets/logos/plus.svg';
import modifyFeatureture from '../../assets/logos/edit.svg';
import contributeFeature from '../../assets/logos/love.svg';
import { Card, CardBody, Row, Col } from 'reactstrap';

import * as constants from '../../constants/languages/cro';

class Dashboard extends Component {
  render() {
    const heroStyles = {
      padding: '50px 0 70px'
    };

    return (
      <div>
        <Row>
          <Col md={10}>
            <div className="home-hero" style={heroStyles}>
              <h1>{constants.dashboard_welcome}</h1>
              <p className="text-muted">
                {constants.dashboard_welcome_sub}
              </p>
            </div>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <Card>
              <CardBody className="display-flex">
                <img
                  src={quizzesFeature}
                  style={{ width: 50, height: 50 }}
                  alt="react.js"
                  aria-hidden={true}
                />
                <div className="m-l">
                  <h2 className="h4">{constants.dashboard_quizzes_head}</h2>
                  <p className="text-muted">
                  {constants.dashboard_quizzes_body}
                  </p>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col md={6}>
            <Card>
              <CardBody className="display-flex">
                <img
                  src={createFeature}
                  style={{ width: 50, height: 50 }}
                  alt="Bootstrap"
                  aria-hidden={true}
                />
                <div className="m-l">
                  <h2 className="h4">{constants.dashboard_create_head}</h2>
                  <p className="text-muted">
                  {constants.dashboard_create_body}
                  </p>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <Card>
              <CardBody className="display-flex">
                <img
                  src={modifyFeatureture}
                  style={{ width: 50, height: 50 }}
                  alt="Sass"
                  aria-hidden={true}
                />
                <div className="m-l">
                  <h2 className="h4">{constants.dashboard_modify_head}</h2>
                  <p className="text-muted">
                  {constants.dashboard_modify_body}
                  </p>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col md={6}>
            <Card>
              <CardBody className="display-flex">
                <img
                  src={contributeFeature}
                  style={{ width: 50, height: 50 }}
                  alt="Responsive"
                  aria-hidden={true}
                />
                <div className="m-l">
                  <h2 className="h4">{constants.dashboard_contribute_head}</h2>
                  <p className="text-muted">
                  {constants.dashboard_contribute_body}
                  </p>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Dashboard;
