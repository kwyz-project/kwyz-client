import React from 'react';
import { Row, Col } from 'reactstrap';
import image from '../../assets/images/traffic-barrier.png'

const BlankPage = () => {
    return (
        <div>
            <Row className="justify-content-center text-muted" style={{ paddingTop: '10%'}}>
                <Col md={4} style={{ display: "table-cell", verticalAlign: "middle", textAlign: "center"}}>
                    <h2>Under construction. Stay tuned!</h2>
                    <img className="pt-5" src={image} alt="Work in progress." width="300" height="300" />
                </Col>
            </Row>
        </div>
    );
};

export default BlankPage;
