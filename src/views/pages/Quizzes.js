import React, { useState, useEffect } from 'react';
import QuizList from '../components/QuizList';
import QuizGame from '../components/QuizGame';
import _ from 'lodash';
import database from '../../database';

const Quizzes = () => {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);
    const [filteredItems, setfilteredItems] = useState([]);
    const [quizClicked, setQuizClicked] = useState(null);

    useEffect(() => {
        var query = database.ref('quizzes').orderByKey();
        query.once('value').then(function(snapshot) {
            setIsLoaded(true);

            let payload = [];
            snapshot.forEach(childSnapshot => {
                payload.push(childSnapshot.val().payload);
            });

            snapshot.hasChildren() ? setItemsState(payload) : setError(error);
        });
    }, []);

    function setItemsState(payload) {
        setItems(payload);
        setfilteredItems(payload);
    }

    function filterItems(value) {
        setfilteredItems(
            _.filter(items, item => {
                return _.includes(_.toLower(item.name), _.toLower(value));
            })
        );
    }

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else {
        if (!quizClicked) {
            return <div>{!quizClicked && <QuizList items={filteredItems} onFilter={filterItems} onClick={setQuizClicked} />}</div>;
        } else {
            return (
                <div>
                    <QuizGame questions={items.find(item => item.id === quizClicked).questions} resetComponent={setQuizClicked} />
                </div>
            );
        }
    }
};

export default Quizzes;
